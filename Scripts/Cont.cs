using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cont : MonoBehaviour
{
    public GameObject control, contenido, contenidoCriterios;
    public Canvas canvasRegistro, canvasMain, canvasCriterios, canvasCriteriosReg, canvasHitos;
    public float contenedor, contenedor2;
    public InputField nombre, nombre2;

    private void Start()
    {
        contenido = GameObject.Find("Contenido");
        contenidoCriterios = GameObject.Find("ContenidoCriterios");
        control = GameObject.Find("control");
        canvasRegistro.enabled = false;
        canvasCriterios.enabled = false;
        canvasCriteriosReg.enabled = false;
        canvasHitos.enabled = false;
    }

    public void GestSeg()
    {
        contenedor = GameObject.Find("Contenido").transform.GetComponent<RectTransform>().sizeDelta.y;
        contenedor += 210;
        contenido.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contenedor);
        GameObject.Find("NuevoSeguimiento").name = nombre.text;
        GameObject.Find("estudiante").GetComponent<Text>().text = nombre.text;
        GameObject.Find("estudiante").name = "t" + nombre.text;
        GameObject.Find("sestudiante").name = "s" + nombre.text;
        GameObject.Find("HitosCanvas").SetActive(true);
        Instantiate(GameObject.Find("HitosCanvas"));
        GameObject.Find("HitosCanvas").name = "HitosCanvas" + nombre.text;
        GameObject.Find("HitosCanvas" + nombre.text).GetComponentInChildren<Text>().text = "Seguimiento de " + nombre.text;
        nombre.text = "";
        canvasRegistro.enabled = false;
        GameObject.Find("HitosCanvas" + nombre.text).GetComponent<Canvas>().enabled = false;
    }

    public void GestApartado()
    {
        contenedor2 = GameObject.Find("ContenidoCriterios").transform.GetComponent<RectTransform>().sizeDelta.y;
        contenedor2 += 160;
        contenidoCriterios.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contenedor2);
        GameObject.Find("NuevoApartado").name = nombre2.text;
        GameObject.Find("nombreapartado").GetComponent<Text>().text = nombre2.text;
        GameObject.Find("nombreapartado").name = "t" + nombre2.text;
        GameObject.Find("%apartado").name = "%" + nombre2.text;
        GameObject.Find("nuapartado").name = "s" + nombre2.text;
        nombre2.text = "";
        canvasCriteriosReg.enabled = false;
    }
}
