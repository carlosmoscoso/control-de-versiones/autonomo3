using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Calificar : MonoBehaviour
{
    public GameObject criterios;
    public int calificacion;
    public Text nota;
    public bool flag = false;
    public void GetCalificacion()
    {

        calificacion = 0;
        foreach (Transform child in criterios.transform)
        {
            if (child.transform.GetSiblingIndex() != child.transform.childCount - 1)
            {
                foreach (Transform hijo in child)
                {
                    if (hijo.transform.GetSiblingIndex() == 0 || hijo.transform.GetSiblingIndex() == child.transform.childCount - 1)
                    {

                    }
                    else
                    {
                        if (hijo.transform.GetChild(2).GetComponent<Toggle>().isOn)
                        {
                            calificacion += int.Parse(hijo.transform.GetChild(1).GetChild(1).GetComponent<InputField>().text);
                        }
                    }
                }
            }
        }
        if (!flag)
        {
            flag = true;
            nota.transform.gameObject.SetActive(true);
            float contenedor = criterios.transform.parent.parent.transform.GetComponent<RectTransform>().sizeDelta.y;
            criterios.transform.parent.parent.transform.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contenedor - 60);
        }
        nota.text = "Ha obtenido un " + ((float)calificacion / 10);
    }
}
